## Vorbereitung eines Projekts

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/112061053578771195</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/projekt-vorbereitung</span>


> **tl/dr;** _(ca. 8 min Lesezeit): In der Phase vor dem eigentlichen Projektstart gibt es schon einiges zu tun: den Bedarf identifizieren, die Voraussetzungen prüfen, herausfinden, ob das Projekt durchführbar und lohnenswert ist. Auf Basis dieser im Business-Case und der Projektskizze festgehaltenen Information wird geprüft, ob mit dem Projekt überhaupt begonnen wird._

## Projekte?

Welche charakteristischen Eigenschaften hat ein Projekt?

Welche drei wesentlichen Zielkategorien unterscheidet man im Projektmanagement?

Warum spricht man häufig von einem "magischen Dreieck" des Projektmanagements? Was daran ist "magisch"?


### Planning Poker

Welche Aussagen treffen zu Planning Poker zu? Zutreffende bitte ankreuzen, es können mehrere oder keine Antwort zutreffen.
	(____P von 3P)
	Mit Planning Poker kann ich exakte Zeiten bestimmen, die ich für die Umsetzung einer Anforderung benötige. 

	Die Einheit von Storypoints lässt sich direkt in Stunden/Tage umrechnen. 

	Nach jeder Pokerrunde legen immer alle die Gründe offen, wie sie zu dem jeweiligen Punktwert gekommen sind. 

	Bei Planning Poker ist wichtig, dass alle Teammitglieder dieselbe fachliche Erfahrung haben. 


Die Einheit von Storypoints ist ein Mass für Komplexität oder Aufwand einer Aufgabe.

Planning Poker eignet sich, um Aufwände unterschiedlicher Tasks relativ zueinander zu ermitteln.

 
### Entwurf BusinessCase

Für das Projekt soll ein BusinessCase entworfen werden. Hierfür sollen Sie den Output, Outcome und Benefit des Projekts darstellen. Beschreiben Sie die drei Begriffe in jeweils einem Satz allgemein und nennen dann ein konkretes Beispiel für die Aufgabenstellung aus Aufgabe 2:

Output <button onclick="toggleAnswer('bc1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bc1">

das Projektprodukt

</span>


Outcome <button onclick="toggleAnswer('bc2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bc2">

Ergebnis, die Geschäftsänderungen, die durch das Projekt erreicht werden (Bearbeitungszeit reduziert sich, bestimmte Probleme sind lösbar,…)

</span>

Benefit <button onclick="toggleAnswer('bc3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bc3">

Benefit ist der eigentliche messbare Nutzen: der Fortschritt durch die Geschäftsänderungen, der ein strategisches Ziel abdeckt und oft erst nach Abschluss des Projekts messbar ist (Kosteneinsparung => Gewinnsteigerung, Nachhaltigkeit, Imagezugewinn,…)

</span>


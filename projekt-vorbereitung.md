## Vorbereitung eines Projekts

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/112061053578771195</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/projekt-vorbereitung</span>


> **tl/dr;** _(ca. 8 min Lesezeit): In der Phase vor dem eigentlichen Projektstart gibt es schon einiges zu tun: den Bedarf identifizieren, die Voraussetzungen prüfen, herausfinden, ob das Projekt durchführbar und lohnenswert ist. Auf Basis dieser im Business-Case und der Projektskizze festgehaltenen Information wird geprüft, ob mit dem Projekt überhaupt begonnen wird._

### Von der Idee zum Projekt

Zu Beginn steht oft nur eine vage Idee im Raum. Diese Idee beschreibt einen Bedarf der Organisation (z.B. das Unternehmen, den Kunden), der durch Innovation, Optimierungsvorschläge, Anpassung an ein verändertes Umfeld (z.B. eine Gesetzesänderung) oder aus anderen Gründen geweckt wurde.

Wenn der Bedarf an eine budgetverantwortliche Stelle (Unternehmensmanagement, Programmmanagement) herangetragen wird, kann daraus ein Projekt erwachsen.

Zu Beginn ist der Bedarf nur unspezifisch ausformuliert und meist noch nicht wirklich verstanden. 

Daher gilt es in der Phase vor dem eigentlichen Projektstart zunächst darum, ausreichend Informationen über diesen Bedarf zu sammeln. Die vorliegenden Informationen müssen detailliert genug sein, um beschließen zu können, ob es sich (geschäftlich) lohnt, diese Idee weiterzuverfolgen. Um es gleich zu sagen: an dieser Stelle muss noch nicht festgelegt werden, ob das Projekt am Ende auch realisiert wird. Denn ein Projektabbruch ist auch später jederzeit möglich (und immer dann geboten, wenn die geschäftliche Rechtfertigung wegbricht).

In großen Organisationen wird bei umfangreichen Projektideen diese Prüfung vonseiten der Unternehmens- oder Programmleitung (bzw. eines Kunden) durch ein Projektmandat angestoßen. Häufig wird damit bereits die zukünftige Projektleitung vertraut (auch wenn es das Projekt selbst noch nicht gibt).

Im folgenden müssen also die Fragen beantwortet werden:

- Handelt es sich um ein Projekt?

- Welche Produkte sollen innerhalb des Projekts erstellt werden?

- Passt das Projekt in die Organisation (das Unternehmen / die Abteilung)?

- Gibt es Vorerfahrungen, auf die aufgebaut werden kann?

- Sind die Projektrollen vergeben?

- Ist das Projekt realisierbar?

- Ist das Projekt lohnenswert (_Business-Case_)?

- Sind alle Voraussetzungen für den Projektstart erfüllt?

Abschließend wird ein Projektsteckbrief erstellt.

### Handelt es sich überhaupt um ein Projekt?

Zunächst muss geklärt werden, ob es sich bei dem Vorhaben überhaupt um ein Projekt oder um eine alltägliche Linientätigkeit handelt. Woran erkennt man ein Projekt? Das Projektmanagement Prince2 definiert den Projektbegriff so^[Prince2-Gossar: [https://www.axelos.com/resource-hub/glossary/prince2-6th-edition-glossaries-of-terms](https://www.axelos.com/resource-hub/glossary/prince2-6th-edition-glossaries-of-terms)]:

> Eine für einen befristeten Zeitraum geschaffene Organisation, die für den Zweck geschaffen wird, ein oder mehrere Produkte in Übereinstimmung mit einem vereinbarten _Business Case_ zu liefern.

Die wesentlichen Projekteigenschaften sind also:

- Das Vorhaben ist **zeitlich begrenzt**.

- Ein Projekt ist häufig **interdisziplinär** und erstreckt sich über mehrere Bereiche / Abteilungen.

- Jedes Projekt ist **einzigartig**, auch wenn sich manche Projekte ähneln.

- Es findet eine **Veränderung** statt: das Projektziel ist ein neuer Soll-Zustand.

- Projekte finden in einem **unsicheren** Umfeld statt und bergen Risiken.

Ein Projekt zeichnet sich durch drei Zielkategorien auf, die jeweils optimiert werden sollten: Zeit, Kosten und Qualität.

Durch die Begrenzungen eines Projekts ergibt sich ein Optimierungsproblem, dass man grafisch mit dem "magischen Dreieck" beschreibt:

Von den drei Zielkategorien können immer nur zwei optimiert werden: will man maximale Qualität sehr schnell, müssen sehr viele Personalstunden parallel in das Projekt fließen: es wird sehr teuer. Benötigt man schnell ein kostengünstiges Produkt, so muss an der Qualität gespart werden.

![Das magische Dreieck mit fixierter Qualität und flexiblen Kosten und Zeit](images/magischesDreieckKlassisch.png){#id .class max-width=30%}

Im klassischen Vorgehen (z.B. nach Wasserfall) schreibt das Lastenheft die Qualität fest. Häufig bleiben die Zielkategorien Zeit und Budget flexibel und laufen aus dem Ruder.

![Das magische Dreieck mit fixierten Kosten und Zeit](images/magischesDreieckAgil.png){#id .class max-width=30%}

Das agile Vorgehen dreht den Spieß um: hier geben die Sprints einen festen Zeit- (und Kosten-)Rahmen vor - es kann aber noch nicht gesagt werden, in welchem Umfang (bzw. mit welcher Qualität) nach dieser Zeit geliefert werden kann.

Die Darstellung mit drei Zielkategorien ist natürlich eine Vereinfachung. Daher gehen einige Projektmanagement-Frameworks wie Prince2 auch einen Schritt weiter und definieren drei weitere Zielkategorien:

- _Kosten_ und _Zeit_ bleiben danach als Zielkategorie erhalten,

- _Qualität_ bezieht sich nur noch auf den Grad der Umsetzung der Anforderungen,

- die Anforderungen selbst werden im _Umfang_ erfasst,

- das am Ende messbare Ergebnis (der _Benefit_) wird in der Zielkategorie _Nutzen_ zusammengefasst und

- die Ereignisse, die mit einer (ggf. unbekannten) Wahrscheinlichkeit eintreten und Auswirkungen auf das Projektergebnis haben, werden unter "Risiko" zusammengefasst.

![Das magische Dreieck erweitert um Umfang, Nutzen und Risiko](images/magischesDreieckPrince2.png){#id .class max-width=30%}

### Welche Produkte sollen innerhalb des Projekts erstellt werden?

Es entstehen eine ganze Menge Produkte innerhalb eines Projekts. Neben dem eigentlichen Produkt, für das das Projekt aufgesetzt wurde, sind dies einige Sekundärprodukte: beispielsweise Pflichtenhefte, Userstories, Pläne oder Protokolle. Wird ein Vorgehen nach einem gängigen Projektmanagement gewählt, müssen zudem eine ganze Reihe Managementdokumente ausgefertigt werden. Um begrifflich das eigentliche Projektprodukt von den anderen Produkten abzugrenzen, spricht man hier vom Spezialistenprodukt. 

Für die Spezialistenprodukte sollte bereits im Vorfeld des Projekts festgehalten werden, welchen Umfang es haben soll.


### Passt das Projekt in die Organisation (das Unternehmen, die Abteilung)?

Organisationen (z.B. Unternehmen) verfolgen bestimmte Ziele - und die Aktivitäten einer Organisation müssen zur Zielerreichung beitragen. Das gilt auch für Projekte: diese müssen stets auf Ziele der Organisation ausgerichtet sein. Es ist Aufgabe der Projektmanagerin bzw. des Projektmanagers, beständig zu prüfen, ob das Projekt noch einen Beitrag zu diesen Zielen leistet.

Die meisten Unternehmen veröffentlichen ihre Unternehmensziele auf deren Website. Häufig ist die Unternehmsausrichtung auf unterschiedlichen Ebenen festgeschrieben:

- Vision: Unternehmensvisionen sind häufig **emotional aufgeladene**, relativ abstrakt formulierte **Zukunftsbilder**. Die Vision von Wikipedia ist beispielsweise: "Imagine a world in which every single human being can freely share in the sum of all knowledge. That's our commitment. "^[https://meta.wikimedia.org/wiki/Vision]

- Mission: zeigt **wertorientiert** Wege auf, wie das Unternehmen im Hier und Jetzt darauf hinarbeitet, die Vision zu erreichen. Vision und Mission bilden gemeinsam das **Unternehmensleitbild**, das nach innen und außen Orientierung zur Ausrichtung des Unternehmens geben soll. Die Mission von Wikipedia lasst sich [hier nachlesen](https://meta.wikimedia.org/wiki/Mission)

- Die Unternehmensstrategien weisen **konkrete Wege** aus, wie sich das Unternehmen in den **Haupthandlungsfeldern** entwickeln will. Diese sind häufig schon als prüfbare Zielformulierungen niedergeschrieben.

- Größere Unternehmen brechen die Strategien für einzelne Geschäftsbereiche (oder Abteilungen) herunter und formulieren strategische Ziele. Das kann sich über mehrere Hierarchiestufen wiederholen.

- Auf der untersten Ebene finden sich die Projekte, die zur Erreichung einzelner oder mehrerer strategischer Ziele beitragen müssen. Projekte können auch strategische Ziele unterschiedlicher Bereiche betreffen.

![Von der Unternehmensvision über das Leitbild zur Strategie bis zu den Projektzielen (Pyramide)](images/unternehmensstrategie.png){#id .class max-width=70%}

Es ist zur Formulierung des _Business-Case_ (s.u.) hilfreich, sich mit den Unternehmenszielen befasst zu haben und zu wissen, zu welchen strategischen Zielen das Projekt beiträgt.

### Gibt es Vorerfahrungen, auf die aufgebaut werden kann?

Als lernende Organisation ist es wichtig, bereits vor Beginn eines Projekts zu recherchieren: 

- Gibt es im Unternehmen einen zentralen Geschäftsprozess für Lessons-Learned-Meetings und deren Dokumentation?

- Welche vergleichbaren Projekte liefen bereits? Wer sind Ansprechpartner?

- Welche formellen Dokumentationen der Erfahrungen liegen hierfür vor? Wie sind diese aufgrund der abweichenden Rahmendaten einzuordnen?

- Wer könnte informelle Erfahrungsberichte beitragen? Mindestens sollten bereits die Gesprächspartner identifiziert werden, erste Gespräche im Vorfeld sind ratsam.

In diesem Zusammenhang ist es wichtig, bereits ein eigenes Erfahrungsprotokoll zu erstellen, in dem über den gesamten Projektverlauf die "Lessons Learned" zusammengetragen werden.


### Sind die Projektrollen vergeben?

Alle Projekte benötigen mindestens zwei Rollen:

- die Projektmanangerin / der Projektmanager

- die Auftraggeberin / der Auftraggeber

Wird das Projekt nach der Prince2-Methodik durchgeführt, so tritt an die Stelle des Auftraggebers der **Lenkungsausschuss** (_project board_). Er ist Gesamtverantwortlich für das Projekt und gibt Entscheidungstoleranzen an den Projektmanager/ die Projektmanagerin weiter. Der Lenkungsausschuss besteht aus Benutzervertreter (ggf. mehrere, benennen Anforderungen), Lieferantenvertreter und Unternehmensvertreter (Auftraggeber, Budgetverantwortliche).

### Ist das Projekt realisierbar?

Projekte finden nie auf der grünen Wiese statt, sondern müssen immer in einem begrenzten und unsicheren Umfeld umgesetzt werden.

Dabei kann es vorkommen, dass einzelne Ressourcen, Informationen oder Verträge die Umsetzung nicht nur erschweren, sondern komplett unmöglich machen. 

### Ist das Projekt lohnenswert (_Business-Case_ entwerfen)?

Bereits bevor mit dem Projekt begonnen wird sollte ein erster Entwurf des _Business-Case_ erstellt werden. Mit diesem Dokument, dass über die gesamte Projektlaufzeit gepflegt werden muss, wird immerwährend dokumentiert und geprüft, ob die geschäftliche Rechtfertigung des Projekts noch gegeben ist. Es muss Zeitaufwand, Kosten, Nutzen und Risiken enthalten.

Projekte, deren geschäftliche Rechtfertigung obsolet geworden ist, müssen abgebrochen werden. Ziel sollte es sein, gerade so viel Information zusammenzutragen, um vorab abschätzen zu können, ob das Projekt lohnenswert ist. Es reicht daher in dieser Phase, den _Business Case_ in einem Entwurfsstadium auszufertigen und die relevanten Werte grob abzuschätzen. 

Der _Business-Case_ sollte in diesem frühen Entwurf mindestens die Projektbegründung und die Realisierungsoptionen umfassen, die anderen Felder können leer gelassen oder (falls vorhanden) mit ersten Schätzungen gefüllt und später präzisiert werden.

|Business-Case||
|---:|:---|
|**Projektname**||
|**Projektsponsor**|_Wer tritt als Auftraggeber in dem Projekt auf? Wer übernimmt die Kosten und zieht den Nutzen aus dem Projekt?_|
|**Projektmanagerin**<br/>**Projektmanager**|_Wem wird die Befugnis erteilt, das Tagesgeschäft und die Projektprodukte zu managen?_|
|**Projektbegründung**|_Was ist der auslösende Bedarf, dem das Projektmandat zugrunde liegt?<br/> Welches Unternehmensziel steht im Fokus?_|
|**Projektnutzen**|_Hier wird Bezug auf strategische Ziele der Organisation (oder des Bereichs) genommen: der **Benefit** ist der eigentliche (oft erst später messbare) Nutzen des Projekts (und nicht das Projektprodukt oder die dadurch erzielten Geschäftsänderungen selbst)._|
|**negative Nebeneffekte**|_Die Realisierung eines Projekts kann auch negative Auswirkungen auf andere Bereiche haben. Welche dieser Nebeneffekte sind jetzt schon bekannt?_|
|**Realisierungsalternativen**<br/> Projektlösungsansatz|_Gibt es mehrere Optionen, den Bedarf zu decken? In der Regel gibt es hier mindestens drei Alternativen:<br/>- "Make": das Projekt selbst durchführen.<br/>- "Buy": das Projektergebnis (oder einen Teil davon) durch Dritte bereitstellen lassen<br/>- das Projekt nicht durchführen.<br/>- ggf. gibt es Alternativen, für die eigene Business-Cases erstellt werden<br/>Für jede Alternative sollte kurz die Wege aufgezeigt werden, die dann zu gehen wären. Die Wahl der Alternative sollte zudem begründet dargelegt werden._|
|**Zeitplan**<br/>|_Aktueller Stand der Zeitaufwandsschätzung<br/>Wesentliche Termine im Projektablauf<br/>Ggf. als Gantt oder Netzplan realisiert.<br/>Mindestens sollte hier der geplante Projektstart und das voraussichtliche Projektende verzeichnet werden, ggf. auch bereits bekannte Meilensteine._|
|**Projektkosten**|_Die Kostenplanung erfolgt zunächst grob auf Basis der geschätzten Personal-, Betriebsmittel-, Werkstoff- und Kapitalkosten. Sie wird im Projektverlauf weiter präzisiert._|
|**Wirtschaftlichkeit**<br/> Interner Zinssatz oder Return of Investment (ROI)<br/>Sensitivitätsanalysen|_Kennziffern zur Ermittlung der Wirtschaftlichkeit sollten hier angeben werden. Ab welchem Zeitpunkt wird erwartet, dass das Projekt kostendeckend arbeitet? Wichtig ist hierbei, dass auch Unsicherheiten in Form von Sensitivitätsanalysen mit angegeben werden._|
|**Risiken**|_Da Projekte in unsicheren Umgebungen stattfinden, ist es wichtig, die Hauptrisiken im Business-Case zu erfassen. Welche Risiken gefährden das Erreichen der Projektziele?_|
|**Budget**|_Welches Budget ist für das eigentliche Projekt sowie die Risiken und erwarteten Änderungen nötig?_|

Im Rahmen des Business-Case sollten drei Begriffe gegeneinander abgegrenzt werden:

- Der **Output** ist die Gesamtheit der Projektprodukte, beispielsweise eine erstellte WebApp.

- Der **Outcome** ist das Ergebnis oder die Geschäftsänderung, die durch den _Output_ (die Produkte) erreicht wird. Beispiel: Der _Output_ WebApp sorgt als _Outcome_ für geänderte Geschäftsprozesse.

- Der **Benefit** ist der eigentliche messbare Nutzen des _Outcomes_: Die neuen Geschäftsprozesse erzielen Fortschritte, die strategische Ziele der Organisation fördern. Beispielsweise können sie dafür sorgen, dass ein Prozess wirtschaftlicher (strategisches Ziel: Businessvalue-Steigerung), schneller (strategisches Ziel: Time-to-Market), nachhaltiger (strategisches Ziel: Dekarbonisierung), sicherer oder fehlerfreier (strategisches Ziel: Compliance-Steigerung) abläuft. Dieser Benefit kann über Metriken bestimmt werden, jedoch oft erst weit nach Projektende.

### Sind alle Voraussetzungen für den Projektstart erfüllt?

Wenn die obigen Punkte abgearbeitet sind, sollten zumindest aus Sicht des Projektmanagements alle Voraussetzungen für einen Projektstart erfüllt sein. Gibt es noch weitere Abhängigkeiten, die überprüft werden müssen? Vor allem bei Teilprojekten eines größeren Programms müssen externe Abhängigkeiten überprüft werden.


### Erstellen des Projektsteckbrief (auch: Projektskizze oder Projektbeschreibung)

In der Projektbeschreibung sollten sich alle Informationen wiederfinden, die in der Vorbereitungsphase zusammengetragen wurden. Ihr zentraler Zweck ist es, dass der Lenkungsausschuss entscheiden kann, ob das Projekt initiiert wird. Daher ist der erstellte _Business-Case_-Entwurf ein wesentlicher Bestandteil des Projektsteckbriefs. Daneben sollten vor allem noch die Projektrollen festgehalten werden (z.B. durch ein Organigramm) und die Projektprodukte beschrieben werden.

|Projektsteckbrief||
|---:|:---|
|**Business-Case**|Projektname, Projektsponsor, Projektmanager*in, Projektbegründung, Projektnutzen, negative Nebeneffekte, Realisierungsalternativen, Zeitplan, Projektkosten, Wirtschaftlichkeit, Risiken, Budget|
|**Projektrollen / Organigramm**|_Auftraggeber, Projektmanager, Projektteam_|
|**Projektprodukte**|_Liste der Projektprodukte und Verweis auf deren Projektproduktbeschreibungen_|


